package Presentation;

import Bussiness_Logic.Bussiness;
import Data_access.Connect;
import Model.Client;
import Model.Product;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.*;
import java.util.Scanner;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.mysql.cj.protocol.Resultset;

public class OrderManager {
	public String rez;
    
	/*afiseaza din orders*/
	public String select_from_Orders(Connect c) throws SQLException {
		String query = "select *from Order_Final";
		String rez = "";
		c.setStat(c.getCon().createStatement());
		ResultSet rs = c.getStat().executeQuery(query);
		ResultSetMetaData r = rs.getMetaData();
		int col = r.getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= col; i++) {
				if (i > 1) {
					rez += ", ";
				}
				rez += rs.getString(i);
			}
			rez += '\n';
		}
		return rez;
	}
    /*afiseaza din client*/
	public String select_from_Client(Connect c) throws SQLException {
		String query = "select * from Client";
		String rez = "";
		c.setStat(c.getCon().createStatement());
		ResultSet rs = c.getStat().executeQuery(query);
		ResultSetMetaData r = rs.getMetaData();
		int col = r.getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= col; i++) {
				if (i > 1) {
					rez += ", ";
				}
				rez += rs.getString(i);
			}
			rez += '\n';
		}
		return rez;
	}
    /*afiseaza din produs*/
	public String select_from_Product(Connect c) throws SQLException {
		String query = "select * from Product";
		String rez = "";
		c.setStat(c.getCon().createStatement());
		ResultSet rs = c.getStat().executeQuery(query);
		ResultSetMetaData r = rs.getMetaData();
		int col = r.getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= col; i++) {
				if (i > 1) {
					rez += ", ";
				}
				rez += rs.getString(i);
			}
			rez += '\n';
		}
		return rez;
	}
    /*mainu*/
	public static void main(String[] args) throws ClassNotFoundException, SQLException, FileNotFoundException {
		Connect c = new Connect();
		c.connection_database();
		OrderManager o = new OrderManager();
		o.rez = "";
		c.con = DriverManager.getConnection(c.url, c.uid, c.pw);
		Bussiness bus = new Bussiness();
		Document doc=new Document();
		File f = new File(args[0]);
		Scanner s = new Scanner(f);
		int index1 = 0, index2 = 0, index3 = 0,index4=0;
		while (s.hasNextLine()) {
			String[] split_vec = new String[4];
			String str = s.nextLine();
			split_vec = str.split("[,:]");
			for (int j = 0; j < split_vec.length; ++j) {
				split_vec[j] = split_vec[j].trim();
			}
			if (split_vec[0].equals("Insert client")) {
				Client cl = new Client(0, split_vec[1], split_vec[2]);
				bus.add_Client(cl, c);
			} else if (split_vec[0].equals("Insert product")) {
				Product p = new Product(0, split_vec[1], Integer.valueOf(split_vec[2]), Double.valueOf(split_vec[3]));
				bus.add_Product(p, c);
			} else if (split_vec[0].equals("Order")) {
				String q = "select * from Client where nume=" + "'" + split_vec[1] + "'";
				String q1 = "select * from Product where nume=" + "'" + split_vec[2] + "'";
				c.setStat(c.getCon().createStatement());
				ResultSet rs = c.getStat().executeQuery(q);
				rs.next();
				int id = rs.getInt(1);
				String nume = rs.getString(2);
				String oras = rs.getString(3);
				Client cl = new Client(id, nume, oras);
				rs = c.getStat().executeQuery(q1);
				rs.next();
				id = rs.getInt(1);
				nume = rs.getString(2);
				int quant = rs.getInt(3);
				double pr = rs.getDouble(4);
				Product p = new Product(id, nume, quant, pr);
				bus.get_order(cl, p, Integer.valueOf(split_vec[3]), c, o);
				try {
					String rez = o.select_from_Orders(c);
					// String rez1 = o.rez;
					
					PdfWriter.getInstance(doc, new FileOutputStream("Bill.pdf"));
					doc.open();
					Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
					Paragraph paragraph = new Paragraph(rez, font);
					doc.add(paragraph);
					
					
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} else if (split_vec[0].equals("Delete client")) {
				String q = "select * from Client where nume=" + "'" + split_vec[1] + "'";
				c.setStat(c.getCon().createStatement());
				ResultSet rs = c.getStat().executeQuery(q);
				rs.next();
				int id = rs.getInt(1);
				String nume = rs.getString(2);
				String oras = rs.getString(3);
				Client cl = new Client(id, nume, oras);
				bus.remove_CLient(cl, c);
			} else if (split_vec[0].equals("Delete product")) {
				String q = "select * from Product where nume=" + "'" + split_vec[1] + "'";
				c.setStat(c.getCon().createStatement());
				ResultSet rs = c.getStat().executeQuery(q);
				rs.next();
				int id = rs.getInt(1);
				String nume = rs.getString(2);
				int quant = rs.getInt(3);
				double pr = rs.getDouble(4);
				Product p = new Product(id, nume, quant, pr);
				bus.remove_Product(p, c);
			} else if (split_vec[0].equals("Report client")) {

				try {
					String rez = o.select_from_Client(c);
					// String rez1 = o.rez;
					Document doc1=new Document();
					PdfWriter.getInstance(doc1, new FileOutputStream("Report_client" + index1 + ".pdf"));
					doc1.open();
					Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
					Paragraph paragraph = new Paragraph(rez, font);
					doc1.add(paragraph);
					doc1.close();
					index1++;

				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (split_vec[0].equals("Report product")) {

				try {
					String rez = o.select_from_Product(c);
					Document doc1=new Document();
					// String rez1 = o.rez;
					PdfWriter.getInstance(doc1, new FileOutputStream("Report_product" + index2 + ".pdf"));
					doc1.open();
					Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
					Paragraph paragraph = new Paragraph(rez, font);
					doc1.add(paragraph);
					doc1.close();
					index2++;

				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (split_vec[0].equals("Report order")) {

				try {
					String rez = o.select_from_Orders(c);
					String rez1 = o.rez;
					Document doc1=new Document();
					PdfWriter.getInstance(doc1, new FileOutputStream("Report_order" + index3 + ".pdf"));
					doc1.open();
					Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
					Paragraph paragraph;
					if(rez1.equals("")) {
						paragraph=new Paragraph(rez,font);
					}
					else paragraph=new Paragraph(rez1,font);
					doc1.add(paragraph);
					doc1.close();
					index3++;

				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
         
		}
		doc.close();
	}
}
