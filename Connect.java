package Data_access;

import java.sql.*;


public class Connect {

    public String url="jdbc:mysql://localhost/warehouse";
    public String uid="root";
    public String pw="";
    public Connection con;
    public Statement stat;
    public ResultSet result;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public Statement getStat() {
        return stat;
    }

    public void setStat(Statement stat) {
        this.stat = stat;
    }

    public ResultSet getResult() {
        return result;
    }

    public void setResult(ResultSet result) {
        this.result = result;
    }

    public void connection_database() throws ClassNotFoundException {
        Class.forName("java.sql.Driver");
    }


}
