package Model;

public class Orders {
    private int order_id;
    private String nume_client;
    private String product_nume;
    private int cantitate_order;

    public Orders(int order_id, String nume_client, String product_nume, int cantitate_order) {
        this.order_id = order_id;
        this.nume_client = nume_client;
        this.product_nume = product_nume;
        this.cantitate_order = cantitate_order;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public String getNume_client() {
        return nume_client;
    }

    public void setNume_client(String nume_client) {
        this.nume_client = nume_client;
    }

    public String getProduct_nume() {
        return product_nume;
    }

    public void setProduct_nume(String product_nume) {
        this.product_nume = product_nume;
    }

    public int getCantitate_order() {
        return cantitate_order;
    }

    public void setCantitate_order(int cantitate_order) {
        this.cantitate_order = cantitate_order;
    }
}
