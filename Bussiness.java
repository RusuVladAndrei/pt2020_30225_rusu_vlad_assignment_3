package Bussiness_Logic;

import Data_access.Connect;
import Model.Client;
import Model.Product;
import Presentation.OrderManager;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Bussiness {
	/*adauga client*/
    public void add_Client(Client cl, Connect c) throws SQLException {
        String q="insert into Client(nume,oras) values(" +  "'" +cl.getNume() + "'" +"," + "'" +cl.getOras() +"');";
        c.setStat(c.getCon().createStatement());
        c.getStat().executeUpdate(q);
    }
    /*adauga/updateaza produs*/
    public void add_Product(Product p,Connect c) throws SQLException {
    	String qu = "select * from Product where nume=" + "'" + p.getNume() + "'";
    	c.setStat(c.getCon().createStatement());
    	ResultSet rs=c.getStat().executeQuery(qu);
    	if(rs.next()==false) {
    		String q="insert into Product(nume,cantitate,price) values(" +  "'" +p.getNume() + "'" +"," +p.getCantitate() +"," + p.getPrice() +");";
            c.getStat().executeUpdate(q);
    	}
    	else {    
    		 int cant=rs.getInt(3) + p.getCantitate();
    		 String qq="update Product set cantitate=" + cant + " where nume=" + "'" + p.getNume() + "';";
    		 c.getStat().executeUpdate(qq);
    	}
    	
    }
    /*sterge client*/
    public void remove_CLient(Client cl,Connect c) throws SQLException {
        String q="delete from Client where nume=" + "'" + cl.getNume() + "'" +"and  oras=" + "'"+ cl.getOras() + "'";
        c.setStat(c.getCon().createStatement());
        c.getStat().executeUpdate(q);
    }
    /*sterge produs*/
    public void  remove_Product(Product p,Connect c) throws SQLException {
        String q="delete from Product where nume=" + "'" + p.getNume() + "'";
        c.setStat(c.getCon().createStatement());
        c.getStat().executeUpdate(q);
    }
    /*face comenzi*/
    public void get_order(Client cl,Product p,int cantitate,Connect c,OrderManager o) throws SQLException {
    	String q="select nume from Product where nume=" + "'" + p.getNume() + "'";
        String q1="select cantitate from Product where nume=" + "'" + p.getNume() + "'";
        String q2="select price from Product where nume=" + "'" + p.getNume() + "'";
        c.setStat(c.getCon().createStatement());
        ResultSet rs=c.getStat().executeQuery(q);
        if(rs.next()==false){
            System.out.println("Product not available");
        }
        else{
            rs=c.getStat().executeQuery(q1);
            rs.next();
            int cant=rs.getInt(1);
            rs=c.getStat().executeQuery(q2);
            rs.next();
            double price_aux=rs.getDouble(1);
            if(cant-cantitate<0){
                o.rez+="Not enough " + p.getNume() + " for client:" + cl.getNume();
                return ;
            }
            else{
            	
                System.out.println("Order placed for customer " + cl.getNume());
                String qq="insert into Orders(nume_client,product_nume,cantitate_order) values(" +  "'" +cl.getNume() + "'" +"," + "'" +p.getNume() +"'" + "," + cantitate +");";
                c.getStat().executeUpdate(qq);
                double price=(double)price_aux*cantitate;
                qq="insert into Order_Final(nume_cl,product_n,price) values(" +  "'" +cl.getNume() + "'" +"," + "'" +p.getNume() +"'" + "," + price +");";
             
                c.getStat().executeUpdate(qq);
                int newq=cant-cantitate;
           
                if(newq==0){
                    this.remove_Product(p,c);
                }
                qq="update Product set cantitate=" + newq + " where nume=" + "'" + p.getNume() + "';";
                c.getStat().executeUpdate(qq);
            }
        }
    }
}
