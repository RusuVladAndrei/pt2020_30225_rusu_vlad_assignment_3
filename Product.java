package Model;

public class Product {
    private int id;
    private String nume;
    private int cantitate;
    private double price;

    public Product(int id, String nume, int cantitate, double price) {
        this.id = id;
        this.nume = nume;
        this.cantitate = cantitate;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
